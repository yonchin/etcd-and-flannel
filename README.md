#etcd and flannel

## How to use ?

### etcd node
	1. git clone https://git.oschina.net/yonchin/etcd-and-flannel.git
	2. cd etcd-and-flannel/etcd 
	3. ./start.sh

### docker(or flannel) node
	1. cd etcd-and-flannel/flannel
	2. ./start.sh
