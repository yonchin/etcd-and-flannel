#!/bin/bash
#
if [[ $# < 1 ]];then
	echo "Usage: $0 [local_ipv4_address]"
	exit 1
fi

export HOST_IP=$1

Flannel_Subnet=10.1.0.0/16

docker-compose down && docker-compose up -d 

sleep 3

curl -s -L http://127.0.0.1:2379/v2/keys/ibm.com/network/config -XPUT -d value='{"Network": "'${Flannel_Subnet}'", "SubnetLen": 24, "Backend": {"Type": "vxlan"}}'
