#!/bin/bash
#

if [[ $# < 1 ]];then
        echo "Usage: $0 [etcd_server_ip]"
        exit 1
fi

export Etcd_Server=$1

pgrep docker > /dev/null 2>&1 || service docker start

docker-compose up -d

sleep 3

. /run/flannel/subnet.env

DOCKER_OPTS="--bip=${FLANNEL_SUBNET} --mtu=${FLANNEL_MTU}"

echo """
DOCKER_OPTS=\"$DOCKER_OPTS -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock --api-cors-header=\'*\' --default-ulimit=nofile=8192:16384 --default-ulimit=nproc=8192:16384\"
""" > /etc/default/docker

service docker restart

